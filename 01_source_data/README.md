Files in the 01_source_data folder were created from multiple sources, and often involved manual cleaning. Details of these source data files, including links to primary data sources, are outlined below.

### closure_proposals.xlsx
This file contains entries for every wiki closure proposal, including closure outcomes.
* Source: [meta:Proposals_for_closing_projects/Closed_proposals](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects#Closed_proposals)
* Source: [meta:Proposals_for_closing_projects/Archive](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects/Archive)

### incubator_graduation.tsv
This file contains entries for each Incubator project that has "graduated from" the Incubator (i.e., has been deemed worthy of Wikimedia hosting, and thus receives its own domain and has its content transferred to that domain.)
* Source: [Incubator:Site_creation_log](https://incubator.wikimedia.org/wiki/Incubator:Site_creation_log)
* Manually cleaned

### Other data sources used in this repo include:
* [wmf.mediawiki_history](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Edits/MediaWiki_history)
