# incubator-data-exploration

The purpose of this repo is to document and organize work related to the [Incubator and Language Representation Across Wikimedia Projects](https://meta.wikimedia.org/wiki/Research:Incubator_and_language_representation_across_Wikimedia_projects) research project.

Specifically, this repo contains code to:
* join, clean, and wrangle the data needed to perform relevant analyses (see "02_wrangling_scripts")
* perform analyes of Incubator and language representation (see "04_analysis/notebooks")

## Repository structure
```
The structure of the repository is outlined below.
.
├── 01_source_data: Contains source files needed for running scripts
├── 02_wrangling_scripts: Contains scripts for wrangling source data files, Hive tables, etc., and creating outputs
├── 03_wrangled_data: Contains files created via the wrangling scripts
├── 04_analysis: 
│   └── notebooks: Contains analysis notebooks
│   └── outputs: Contains files (tsv, png, etc) outputted from analysis notebooks
└── README.md
```
Each folder contains its own README providing details about the files in that folder.

## Authors and acknowledgment
Contributors to this project: [@kcvelaga](https://gitlab.wikimedia.org/kcvelaga), [@ilooremeta](https://gitlab.wikimedia.org/ilooremeta), [@hghani](https://gitlab.wikimedia.org/hghani), and [@cmyrick](https://gitlab.wikimedia.org/cmyrick)


