### closed_projects.ipynb
This notebook generates a dataframe with closed wikimedia projects, closure proposal dates, closure or deletion date, projects language, and project type. It does this by parsing the data from closure_proposals.xlsx (from 01_source_data folder), and combining it with extra data from the [canonical-data wiki](https://github.com/wikimedia-research/canonical-data/tree/master/wiki) list.
* Code: Python
* Output produced: **closed_projects_pre_manual_additions.csv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/03_wrangled_data) folder

### closed_projects_add_missing_data.ipynb
This notebook takes the **closed_projects.csv** (output from **closed_projects.ipynb** in this folder), which has many rows with missing data, and adds the missing data in manually. To run the notebook with R, you need to create a conda environment and activate the R kernel so Jupyter can recognize it.
* Code: R
* Output produced: **closed_projects.csv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/03_wrangled_data) folder

### join_countries_wmfregions.ipynb
This notebook generates a dataframe with country names (English), country ISO codes, county's latitude and longitude, and country's WMF Region. For details about WMF regions, see [https://meta.wikimedia.org/wiki/Wikimedia_regions](https://meta.wikimedia.org/wiki/Wikimedia_regions). To run the notebook with R, you need to create a conda environment and activate the R kernel so Jupyter can recognize it.
* Code: R
* Output produced: **countries_wmfregions.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/03_wrangled_data) folder

### language.ipynb
This notebooks generates a dataframe matching all Incubator project prefixes (e.g, "Wt/ig") with its corresponding language name and project type (e.g., "Igbo Wiktionary"). It does this by matching langauge codes with [canonical-data.wikis](https://github.com/wikimedia-research/canonical-data/blob/master/wiki/wikis.tsv) language codes, and then matching remaning codes with [SIL IS0 639-3s](https://iso639-3.sil.org/sites/iso639-3/files/downloads/iso-639-3.tab).
* Code: Python
* Output produced: **incubator_langauges.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
