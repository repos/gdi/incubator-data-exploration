### closed_projects.tsv
This file is the output of **closed_projects_add_missing_data.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder. It contains the names of closed wikimedia projects, closure proposal dates, closure or deletion date, projects language, and project type. It has many cells with missing data. 

For a version of this file from _before_ missing data were manually added, see **closed_projects_pre_manual_additions.tsv** in this folder.

### closed_projects_pre_manual_additions.csv
This file is the output of **closed_projects.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder. It contains the names of closed wikimedia projects, closure proposal dates, closure or deletion date, projects language, and project type. It has many cells with missing data. These data were missing from the original wikitables scraped. 

For a version of this file with the missing data manually added, see **closed_projects.tsv** in this folder.

### countries_wmfregions.tsv
This file is the output of **join_countries_wmfregions.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder. It contains country names (English), country ISO codes, county's latitude and longitude, and country's WMF Region. For details about WMF regions, see https://meta.wikimedia.org/wiki/Wikimedia_regions.

### incubator_languages.tsv
This file is the output of **language.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder. It contains (past and present) Incubator projects' prefixes (e.g., "Wt/ig") with their corresponding language names and project type (e.g, "Igbo Wiktionary").
